const express = require('express');
const router = express.Router();
const User = require('../models/user');

router.get('/register', (req, res) => {
    res.render('register', {msg: null})
});

router.post('/registerUser', (req, res) => {
    if (!req.body.email || !req.body.fullName || !req.body.password) {
        return res.render('register', {msg: "Empty Feild"});
    };

    if (req.body.password.length < 4 || req.body.password.length > 50) {
        return res.render('register', {msg: "err in pass length"});
    };

    if (req.body.fullName.length < 3 || req.body.fullName.length > 30) {
        return res.render('register', {msg: "err in full name length"});
    };

    User.findOne({email: req.body.email.trim()}, (err, existUser) => {
        if (err) {
            return res.render('register', {msg: "Somthing went wrong :("});
        };
        if (existUser) {
            return res.render('register', {msg: "email already exist"});
        };

        new User({
            fullName: req.body.fullName,
            password: req.body.password,
            email: req.body.email
        }).save((err, user) => {
            if (err) {
                return res.render('register', {msg: "Somthing went wrong :("});
            };
    
            res.render('login')
        });
    });
});


router.get('/login', (req, res) => {
    res.render('login', {msg: null})
});

router.post('/loginUser', (req, res) => {
    if (!req.body.email || !req.body.password) {
        return res.render('login', {msg: "Empty Feild"});
    };

    User.findOne({email: req.body.email, password: req.body.password}, (err, user) => {
        if (err) {
            return res.render('login', {msg: "Somthing went wrong :("});
        };
        if (!user) {
            return res.render('login', {msg: 'email or password is invalid'})
        };

        res.render('index', user)
    })
})




module.exports = router;

