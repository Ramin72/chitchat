const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    fullName: {
        type: String,
        required: true,
        trim: true,
        minlength: 3,
        maxlength: 30
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique:true
    }, 
    password: {
        type: String, 
        required: true,
        minlength: 4,
        maxlength: 50
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    avatar: {
        type: String
    }
});

module.exports = mongoose.model('User', UserSchema);